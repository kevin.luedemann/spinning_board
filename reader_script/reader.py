import serial
import struct
import time
import matplotlib.pyplot as plt
import numpy as np

#port        = serial.Serial("/dev/ttyACM0",115200)
port        = serial.Serial("/dev/ttyACM1",115200)
NUM_BYTE    = 16

data        = []
counter     = 0
start       = time.time()
while counter < 5000:
    value = struct.unpack("ffff",port.read(NUM_BYTE))
    data.append(value)
    counter    += 1
stop    = time.time()

duration    = stop-start
print("duration\t= {:.02e}s".format(stop-start))
print("Bytes\t\t= {}Byte".format(counter*NUM_BYTE))
print("Data rate\t= {:.02f}kB/s".format(counter*NUM_BYTE/duration/1000))
print("Transferrate\t= {:.02f}points/s".format(counter/duration))
#print("Transferrate={:.02f}B/s".format(counter*NUM_BYTE/duration))
data        = np.array(data)
g           = np.mean(np.sqrt(data[:,1]**2+data[:,2]**2+data[:,3]**2))
print(g)
print(g/9.80665)

#fig,ax      = plt.subplots()
#ax.plot(data[:,0]-np.ones_like(data[:,0])*data[0,0],data[:,2])
#
#plt.show()
