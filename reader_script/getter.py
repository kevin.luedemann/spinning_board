import serial
import struct
import time
import numpy as np
from datetime import datetime
#import pivresult
import signal, os

interrupt   = False

def handler(signum, frame):
    global interrupt
    print('Signal handler called with signal', signum)
    interrupt   = True
    
signal.signal(signal.SIGINT, handler)

#port        = serial.Serial("/dev/ttyACM0",115200)
port        = serial.Serial("/dev/ttyACM1",115200)
#port        = serial.Serial("COM4",115200)
NUM_BYTE    = 40

#Change this for the experiment
src_dir     = "./"

#Open database connection
#client = pivresult.devices.influxdb_client.create(
#                                                    "134.76.40.95",
#                                                    8086,
#                                                    "experiment")

#open file and write header
fp          = open(os.path.join(src_dir,datetime.strftime(datetime.now(),"%Y%m%d_%H%M%S.txt")),"w")
fistr   = "#Date_time\ttemp1(roh)\ttemp2(roh)\ttemp3(roh)\ttemp1(eich)\ttemp2(eich)\ttemp3(eich)\tgy\tgx\tgz\n"
fp.write(fistr)

#Remove old data from buffer
port.reset_input_buffer()
while not(interrupt):
    #Read in the datapaket
    value   = struct.unpack("ffffffffff",port.read(NUM_BYTE))
    #Format into usable string
    fistr   = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(datetime.strftime(datetime.now(),"%Y%m%d_%H%M%S.%f"),*value[1:])

    #Print to file
    fp.write(fistr)
    fp.flush()

    #Output to database
    #db_output = {"measurement":"temps",
    #            "time":dtime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
    #            "fields":{}}
    #db_output["fields"].update({"temp1":value[1]})
    #db_output["fields"].update({"temp2":value[2]})
    #db_output["fields"].update({"temp3":value[3]})
    #db_output["fields"].update({"resi1":value[4]})
    #db_output["fields"].update({"resi2":value[5]})
    #db_output["fields"].update({"resi3":value[6]})
    #db_output["fields"].update({"g1":   value[7]})
    #db_output["fields"].update({"g2":   value[8]})
    #db_output["fields"].update({"g3":   value[9]})
    #client.write_points([db_output])

    #write last value into file
    with open(os.path.join(src_dir,"current_temps.txt"),"w") as dafi:
        dafi.write(fistr.replace("\t"," "))

    #Print to console
    #print(fistr.replace("\t"," "))

fp.close()

