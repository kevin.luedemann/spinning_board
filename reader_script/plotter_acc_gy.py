import serial
import struct
import time
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np
from datetime import datetime

def get_initial_frame(port,frame_size=500,num_frames=10,plt_axis=1):
    value       = struct.unpack("fffffff",port.read(28))
    data        = np.array([value[0],value[plt_axis]])
    data        = np.expand_dims(data,axis=0)
    data        = np.pad(data,((num_frames*(frame_size)-1,0),(0,0)),mode='edge')
    return data
    
def insert_frame(port,data,frame_size=500,plt_axis=1,fname=""):
    data        = np.roll(data,-frame_size,axis=0)
    if fname != "":
        fi      = open(fname,"a")
    for i in range(frame_size):
        value   = struct.unpack("fffffff",port.read(28))
        if fname != "":
            fi.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(*value))
        data[-frame_size+i,:] = np.array([[value[0],value[plt_axis]]])
    if fname != "":
        fi.close()
    return data

plt_axis    = 1
save_time   = False

port        = serial.Serial("/dev/ttyACM1",115200)
#port        = serial.Serial("COM4",115200)
data        = get_initial_frame(port)
if save_time:
    fname   = "{}_data.txt".format(datetime.strftime(datetime.now(),"%Y%m%d_%H%M%S"))
else:
    fname   = ""
data        = insert_frame(port,data,plt_axis=plt_axis,fname=fname)

fig,ax      = plt.subplots()
li,         = ax.plot(data[:,0],data[:,plt_axis],"r+-")
ax.set_xlim([data[0,0],data[-1,0]])
ax.set_ylim([np.min(data[:,plt_axis]),np.max(data[:,plt_axis])])

def update(frame,plt_axis=1,fname=""):
    global data
    global port
        data    = insert_frame(port,data,plt_axis=plt_axis,fname=fname)
    if plt_axis > 3:
        li.set_data(data[:,0],data[:,plt_axis]/2/np.pi)
        ax.set_ylim([np.min(data[:,plt_axis]/2/np.pi),np.max(data[:,plt_axis]/2/np.pi)])
    else:
        li.set_data(data[:,0],data[:,plt_axis])
        ax.set_ylim([np.min(data[:,plt_axis]),np.max(data[:,plt_axis])])
    ax.set_xlim([data[0,0],data[-1,0]])
    return li,

def on_press(event):
    global data
    print('press', event.key)
    if event.key == 'x':
        output      = np.copy(data)
        output[:,0]-= np.ones_like(output[:,0])*output[0,0]
        np.save("{}_accl.npy".format(datetime.strftime(datetime.now(),"%Y%m%d_%H%M%S")),output)
        fig.savefig("{}_accl.png".format(datetime.strftime(datetime.now(),"%Y%m%d_%H%M%S")),dpi=300,bbox_inches="tight")

fig.canvas.mpl_connect('key_press_event', on_press)
ani         = FuncAnimation(fig,update,blit=False,fargs=(plt_axis,fname))

plt.show()

