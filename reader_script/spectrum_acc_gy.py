import serial
import struct
import time
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.colors import LogNorm
import numpy as np
from numpy.fft import rfft,rfftfreq

def get_initial_frame(port,frame_size=500,num_frames=10,plt_axis=1):
    value       = struct.unpack("fffffff",port.read(16))
    data        = np.array([value[0],value[plt_axis]])
    data        = np.expand_dims(data,axis=0)
    data        = np.pad(data,((num_frames*(frame_size)-1,0),(0,0)),mode='edge')
    return data
    
def insert_frame(port,data,frame_size=500,plt_axis=1):
    data        = np.roll(data,-frame_size,axis=0)
    for i in range(frame_size):
        value   = struct.unpack("fffffff",port.read(16))
        data[-frame_size+i,:] = np.array([[value[0],value[plt_axis]]])
    return data

def get_fft(data,dt=1):
    fft     = rfft(data)
    freq    = rfftfreq(data.shape[0],d=dt)
    return fft,freq

def get_fft_arr(data,frame_size=500,frame_shift=500,remove_avg=True):
    nt      = data.shape[0]
    if remove_avg:
        avg     = np.mean(data[:,1])
    else:
        avg     = 0.0
    fft_arr = [np.abs(rfft( data[i*frame_shift:(i*frame_shift)+frame_size,1]-
               np.ones_like(data[i*frame_shift:(i*frame_shift)+frame_size,1])*avg)) for i in range(int(nt/frame_shift-frame_shift))]
    return np.array(fft_arr)

plt_axis    = 1
num_frames  = 100
frame_size  = 500
frame_shift = int(50)


port        = serial.Serial("/dev/ttyACM1",115200)
#port        = serial.Serial("COM4",115200)
data        = get_initial_frame(port,
                                frame_size=frame_size,
                                num_frames=num_frames,
                                plt_axis=plt_axis)
data        = insert_frame(port,data,frame_size=frame_size,plt_axis=plt_axis)

dt          = data[-1,0]-data[-2,0]
fft,freq    = get_fft(data[-frame_size:,1],dt=dt)

fft_arr     = get_fft_arr(data,frame_size=frame_size,frame_shift=frame_shift)

dt_shift    = data[-1,0]-data[-frame_shift-1,0]
X,Y         = np.meshgrid(freq,np.arange(num_frames*frame_size/frame_shift-frame_shift-1,-1,-1)*dt_shift)

fig,ax      = plt.subplots()
img         = ax.pcolormesh(X,Y,fft_arr,
                            norm=LogNorm(),
                            shading='auto',
                            cmap="inferno")
fig.colorbar(img,ax=ax)
ax.set_xlabel("f/Hz")
ax.set_ylabel("time/s")

def update(frame,frame_size,frame_shift):
    global data
    global port
    global img
    data        = insert_frame(port,data)
    fft_arr     = get_fft_arr(data,frame_size=frame_size,frame_shift=frame_shift)
    img.set_array(fft_arr.flatten())
    img.autoscale()
    return img,

ani         = FuncAnimation(fig,update,blit=False,fargs=(frame_size,frame_shift))

plt.show()
