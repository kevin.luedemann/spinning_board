import board
import busio
import digitalio
import time

import adafruit_mpu6050
#import adafruit_htu21d
import usb_cdc
import struct
import adafruit_max31865
import supervisor

try:
    def send_dataframe(cdc,s1,s2,s3,gy,readable=True):
        if readable:
            packet  = "{}\t{}\t{}\t{}\t{}\t{}\t{}\n\r".format(
                        time.monotonic(),
                        s1[0],
                        s2[0],
                        s3[0],
                        gy[0]*9.5492966,
                        gy[1]*9.5492966,
                        gy[2]*9.5492966
                        )
            packet  = bytearray(packet)
        else:
            packet  = struct.pack("ffffffffff",
                        time.monotonic(),
                        s1[1],
                        s2[1],
                        s3[1],
                        s1[0],
                        s2[0],
                        s3[0],
                        gy[0],
                        gy[1],
                        gy[2]
                        )
        cdc.write(packet)

    i2c     = busio.I2C(board.SCL,board.SDA,frequency=400000)

    #Accel and gyro data
    acc_ok  = False
    try:
        mpu     = adafruit_mpu6050.MPU6050(i2c)
        acc_ok  = True
    except:
        print("Accelerometer not working")

    if acc_ok:
        mpu.accelerometer_range             = adafruit_mpu6050.Range.RANGE_2_G
        mpu.gyro_range                      = adafruit_mpu6050.GyroRange.RANGE_250_DPS
        mpu.sample_rate_devisor             = 1 #1 oder 0 noch klaerename
        mpu.filter_bandwidth                = adafruit_mpu6050.Bandwidth.BAND_260_HZ


    #Room Humidity and temperature
    #room    = adafruit_htu21d.HTU21D(i2c)
    #print("Room={}C,\t{}%".format(room.temperature,room.relative_humidity))

    #spi     = board.SPI()
    spi     = busio.SPI(board.SCK,board.MOSI,board.MISO)
    spi.try_lock()
    spi.configure(baudrate=1000000)
    spi.unlock()

    #PT1000 data
    CS1     = digitalio.DigitalInOut(board.D25)
    CS2     = digitalio.DigitalInOut(board.D24)
    CS3     = digitalio.DigitalInOut(board.A3)
    sensor1 = adafruit_max31865.MAX31865(spi, CS1, rtd_nominal=1000.0, ref_resistor=4300.0, wires=4)
    sensor2 = adafruit_max31865.MAX31865(spi, CS2, rtd_nominal=1000.0, ref_resistor=4300.0, wires=4)
    sensor3 = adafruit_max31865.MAX31865(spi, CS3, rtd_nominal=1000.0, ref_resistor=4300.0, wires=4)
    print("S1={}C,\t{}Ohm".format(sensor1.temperature,sensor1.resistance))
    print("S2={}C,\t{}Ohm".format(sensor2.temperature,sensor2.resistance))
    print("S3={}C,\t{}Ohm".format(sensor3.temperature,sensor3.resistance))
    print()

    ax,ay,az        = 0.0,0.0,0.0
    gx,gy,gz        = 0.0,0.0,0.0

    last    = time.monotonic()
    while True:
        if acc_ok:
            #ax,ay,az                    = mpu.acceleration
            gx,gy,gz                    = mpu.gyro

        #usb_cdc.data.write(struct.pack('ffff',time.monotonic(),gx,gy,gz))

        if time.monotonic() - last >= 1:
            last            = time.monotonic()
            sen1            = [ sensor1.temperature,
                                sensor1.resistance]
            sen2            = [ sensor2.temperature,
                                sensor2.resistance]
            sen3            = [ sensor3.temperature,
                                sensor3.resistance]
            send_dataframe( usb_cdc.console,
                            sen1,
                            sen2,
                            sen3,
                            [gx,gy,gz],
                            readable=True)
            send_dataframe( usb_cdc.data,
                            sen1,
                            sen2,
                            sen3,
                            [gx,gy,gz],
                            readable=False)

except:
    print("Caught some error. Rebooting!")
    supervisor.reload()    

